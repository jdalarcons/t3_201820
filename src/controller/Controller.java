package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {

	}
	
	public static void loadTrips() {
		
	}
		
	public static IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		return null;
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return null;
	}
}
